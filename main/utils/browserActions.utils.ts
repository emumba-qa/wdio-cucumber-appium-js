import * as waitTimeout from "@wdio-utils/waits.json";

let minutes = waitTimeout.maxTimeout / 60000;
let element = 'Element';

export async function dropDownSelect(dropdownSelector, optionSelector) {
  let dropDown = await dropdownSelector;
  await dropDown.click();
  let dropDownOption = await optionSelector;
  await dropDownOption.scrollIntoView();
  await dropDownOption.waitForDisplayed();
  await dropDownOption.click();
}
export async function findElement(using: string, value: string) {
  const elementRef = await browser.findElement(using, value); // Find the element using the provided locator strategy and value
  const element = await $(elementRef); // Convert the element reference to a WebDriverIO Element object
  await element.click(); // Click on the element
}
export async function findElementsFromElement(parentElement, using, value) {
  const parentRef = await parentElement.getRef();
  const elements = await parentElement.$$(using, value, parentRef);
  return elements;
}
export async function sendKeysToElement(element, text) {
  // await element.scrollIntoView() 
  await $(element).waitForDisplayed();
  await $(element).setValue(text);
}
export async function customClick(selector: string) {
await $(selector).waitForExist()
}
export async function click(selector: string, elementName?: string) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.scrollIntoView();
  await browser.execute(arg => arg.click(), await elementHandle);
}
export async function forcedClick(selector: string, elementName?: string) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.click()
}

export async function hoverOver(selector: string, elementName?: string) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.scrollIntoView();
  await elementHandle.waitForDisplayed();
  await elementHandle.moveTo();
}
export async function getTextArray(selector: string) {
  let elementTextArray = await $$(selector).map(el => el.getText());
  let textArray = await Promise.all(elementTextArray);
  return textArray
}
export async function getTextArrayLowerCase(selector: string) {
  let elementTextArray = await $$(selector).map(el => el.getText());
  let textArray = await Promise.all(elementTextArray);
  return textArray.flat().map((text) => text.toLowerCase());
}
export async function getTextElement(selector) {
  let elementText = await $(selector).getText();
  return elementText;
}
export async function getAttributeArray(selector: string, attr: string) {
  let elementAttributeArray = await $$(selector).map(el =>
    el.getAttribute(attr)
  );
  let attributeArray = await Promise.all(elementAttributeArray);
  return attributeArray;
}
export async function getAttribute(
  selector: string,
  attr: string,
  elementName?: string
) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.scrollIntoView();
  let elementAttribute = await elementHandle.getAttribute(attr);
  return elementAttribute;
}
export async function scrollTo(selector: string, elementName?: string) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.scrollIntoView();
}
export async function getElementArray(selector: string, elementName?: string) {
  await scrollTo(selector, elementName);
  let getElementArray = await $$(selector);
  let elementArray = await Promise.all(getElementArray);
  return elementArray;
}

export async function addStaticWait(waitTime) {
  await browser.pause(waitTime)
}
export async function waitAndClick(selector: string, elementName?: string) {
  if (elementName) {
    element = elementName;
  }
  let elementHandle = await $(selector);
  await elementHandle.waitForExist({
    timeoutMsg: `${element} did not exist after ${minutes} minutes`,
  });
  await elementHandle.waitForDisplayed({
    timeoutMsg: `${element} did not Displayed after ${minutes} minutes`,
  });
  await browser.execute(arg => arg.click(), await elementHandle);
}

