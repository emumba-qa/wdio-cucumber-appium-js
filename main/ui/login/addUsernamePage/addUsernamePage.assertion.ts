import * as locators from "@wdio-ui/login/addUsernamePage/addUsernamePage.locators";
import * as assertions from "@wdio-utils/browserAssertions.utils";
import * as waits from "@wdio-utils/waits.json";

export async function verifyEmailPage() {
    await waits.pauseTimeout;
    assertions.verifyElementIsDisabled(locators.getUsernameField,"Email field is disabled", false)
}
export async function verifyContinueButton() {
    await $(locators.getContinueButton).waitForDisplayed({timeout: waits.verySmallWait});
    assertions.verifyElementIsNotClickable(locators.getContinueButton,"Continue Button is not clicked", true)
}