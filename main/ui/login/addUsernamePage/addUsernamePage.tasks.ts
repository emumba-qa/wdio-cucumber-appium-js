import * as locators from "@wdio-ui/login/addUsernamePage/addUsernamePage.locators";
import * as actions from "@wdio-utils/browserActions.utils"

export async function enterUsername(userName) {
    await actions.sendKeysToElement(locators.getUsernameField,userName);
    await actions.forcedClick(locators.getContinueButton);
}