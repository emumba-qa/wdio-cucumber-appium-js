import * as locators from "@wdio-ui/login/signInPage/signInPage.locators";
import * as assertions from "@wdio-utils/browserAssertions.utils";
import * as waits from "@wdio-utils/waits.json";

export async function verifyLoginPage(){
    await waits.pauseTimeout;
    assertions.verifyElementIsDisplayed(locators.getAppLabel,"Main Text is not displayed",false)
}
export async function verifySignInButton(){
    await $(locators.getSignIn).waitForDisplayed({timeout:waits.verySmallWait});
    assertions.verifyElementIsNotClickable($(locators.getSignIn),"Sign In Button is not clicked", true)
}