import * as locators from "@wdio-ui/login/signInPage/signInPage.locators";
import * as actions from "@wdio-utils/browserActions.utils"
import * as waits from "@wdio-utils/waits.json";

export async function clickOnSignIn(){
    await actions.forcedClick(locators.getSignIn)
    await waits.pauseTimeout;
}
