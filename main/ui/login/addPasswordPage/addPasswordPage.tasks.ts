import * as locators from "@wdio-ui/login/addPasswordPage/addPasswordPage.locators";
import * as actions from "@wdio-utils/browserActions.utils"

export async function enterPassword(password){
    await actions.sendKeysToElement(locators.getPasswordField,password);
    await actions.forcedClick(locators.getLogInButton)
}