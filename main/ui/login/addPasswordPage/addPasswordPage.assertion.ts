import * as locators from "@wdio-ui/login/addPasswordPage/addPasswordPage.locators";
import * as assertions from "@wdio-utils/browserAssertions.utils";
import * as waits from "@wdio-utils/waits.json";

export async function verifySignInText(){
    await waits.pauseTimeout;
    assertions.verifyElementIsDisplayed(locators.getSignInText,"Sign In Text is not Displayed",false)
}
export async function verifyPasswordPage(){
    await $(locators.getPasswordField).waitForDisplayed({timeout: waits.verySmallWait});
assertions.verifyElementIsDisabled($(locators.getPasswordField),"Password field is disabled",false)
}
export async function verifyLogInButton(){
    await $(locators.getLogInButton).waitForDisplayed({timeout: waits.verySmallWait});
    assertions.verifyElementIsNotClickable($(locators.getLogInButton),"Login In Button is not clicked", false)
}
export async function verifyStoreText(){
    await $(locators.getStoreText).waitForDisplayed({ timeout: waits.averageWait});
    const actualStoreText = await $(locators.getStoreText).getText();
    assertions.verifyElementsMatches(actualStoreText,"Store","Store text is not displayed",true)
}