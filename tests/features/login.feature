@All
@login
Feature: User should be able to login to the app with email and password  
    Scenario: User logs in to Test app
          Given User lands on the main sign in page
          When User enters username
          Then Verify user lands on password page
          When User enters password
          Then Verify user lands to store page of app