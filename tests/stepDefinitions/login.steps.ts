import {Given,When,Then} from "@wdio/cucumber-framework"
import * as testData from "@wdio-configs/loginCreds.json"
import * as signInPageActions from "@wdio-ui/login/signInPage/signInPage.tasks";
import * as usernameActions from "@wdio-ui/login/addUsernamePage/addUsernamePage.tasks"
import * as passwordActions from "@wdio-ui/login/addPasswordPage/addPasswordPage.tasks"
import * as AppLoginLabel from "@wdio-ui/login/signInPage/signInPage.assertions"
import * as signInText from "@wdio-ui/login/addPasswordPage/addPasswordPage.assertion"

Given("User lands on the main sign in page",async () => {
    await AppLoginLabel.verifyLoginPage();
    await signInPageActions.clickOnSignIn();
});
When("User enters username",async () => {
    await usernameActions.enterUsername(testData.validUser.username);
});
Then("Verify user lands on password page", async() => {
    await signInText.verifySignInText();
});
When("User enters password",async () => {
    await passwordActions.enterPassword(testData.validUser.password);
});
Then("Verify user lands to store page of app", async () => {
    await signInText.verifyStoreText();
});

